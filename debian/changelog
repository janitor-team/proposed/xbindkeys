xbindkeys (1.8.7-1) unstable; urgency=medium

  * New upstream version 1.8.7
  * Switch to dpkg-source 3.0 (quilt) format
  * Update manpages patch for new upstream
  * Update watch URL to use secure protocol
  * Bump compat level to 13 and simplify rules
  * Add Rules-Requires-Root field

 -- Ricardo Mones <mones@debian.org>  Sun, 06 Sep 2020 17:25:17 +0200

xbindkeys (1.8.6-2) unstable; urgency=high

  * Build depend on Guile 2.2.
    Thanks to Martin Samuelsson for the original patch (Closes: #885239)
  * Update homepage URL
  * Update watch file to version 4 and expand regexp
  * Add manpage for xbindkeys_autostart
  * Update copyright to DEP 5 format
  * Set compatibility level to 12
  * Update Standards-Version to 4.5.0, no other changes
  * Add Vcs-* headers for current repository

 -- Ricardo Mones <mones@debian.org>  Thu, 23 Jan 2020 21:09:14 +0100

xbindkeys (1.8.6-1) unstable; urgency=medium

  * New upstream version
    - Bug fix: Do not grab keysym not mapped on keyboard.
      Prevent a AnyKey grab, this should (Closes: #447026)
  * Bug fix: "New release blocks seahorse"  (Closes: #628654).
    We now put the autostart script elsewhere and place a file in
    /etc/xdg/autostart. In the common Desktop Environments this should
    have xbindkeys still start automatically, but without the problems the
    old way of using /etc/X11/Xsession.d gave us.
    Users whose environment does not make use of /etc/xdg/autostart do
    need to manually start xbindkeys, either by just running it from their
    xsession or by executing /usr/bin/xbindkeys_autostart
  * Bug fix: "Please update homepage in man page", thanks to Mathieu
    Malaterre (Closes: #640477).
  * Bug fix: "[xbindkeys] xbindkeys defaults file should only be
    examples", thanks to Dean Evans (Closes: #657965).
  * Update guile version

 -- Joerg Jaspert <joerg@debian.org>  Sun, 16 Feb 2014 13:59:57 +0100

xbindkeys (1.8.5-1) unstable; urgency=low

  * New upstream version (Closes: #628165).
  * Bug fix: "xbindkeys crashes upon second keypress due to memory
    corruption", thanks to Josh D (Closes: #613429).
  * Bug fix: "xbindkeys single-handedly wastes a watt of power polling for
    a nonexistent file", thanks to Steve Langasek (Closes: #581509).
  * Bug fix: "configure.ac has additional whitespaces which confuse
    autoconf", thanks to booiiing (Closes: #537826).
  * Bug fix: "Does not provide a method to run for all users", thanks to
    Evan Giles (Closes: #332319).
  * Bug fix: "Crashes upon Guile GC", thanks to J\.P\. Larocque (Closes:
    #516328).
  * Bug fix: "only the first entry in xbindkeysrc works until the file is
    edited", thanks to Celejar (Closes: #445718).

 -- Joerg Jaspert <joerg@debian.org>  Sat, 28 May 2011 20:35:39 +0200

xbindkeys (1.8.3-1) unstable; urgency=low

  * New upstream version (Closes: #542533)
    - Fix watch file to point current site (Closes: #449685)
    - Added Homepage field to control file.
  * debian/menu
    - Fixed quotation of needs field
    - Section fixed to "Applications/Accessibility" (Closes: #444890)
      While it's a generic tool, not specifically targeted to any subset
      of users, it may certainly help also people with disabilities or
      with unusual systems.
  * Added compat file, set to level 7
  * debian/control
    - Removed obsolete x-dev from Build-Depends: x11-proto-core-dev is
      already pulled by libx11-dev. (Closes: #515463)
    - Set debhelper versioned dependency to match compat
    - Added autotools-dev for fixing ancient config.{sub,guess}
    - Updated Standards-Version to 3.8.3 with no other changes required
    - Added ${misc:Depends} to binary package
    - Added myself to uploaders
  * debian/rules
    - Added targets to fix ancient config.{sub,guess} files
    - Replaced dh_clean -k with dh_prep for compat level 7
    - Reformatted and removed template coments,
  * Fixed changelog spelling errors reported by lintian.

 -- Ricardo Mones <mones@debian.org>  Sat, 16 Jan 2010 00:34:06 +0100

xbindkeys (1.8.2-1) unstable; urgency=low

  * New upstream version (Closes: #346423, #388690)
    - Enable a full access to the xbindkeys internal from the guile
      scheme configuration file. A grabbed key can start a shell command
      or run a scheme function. This enable to do more powerful things
      inside xbindkeys whitout the need of an external shell script
      (like double click, timed double click or keys combinations).
    - Configuration files are reloaded on the fly when they have
      changed so there is no more need to send a HUP signal to reload
      them
    - Changes to grab keys in all display screens and not only in the
      default root window (Xinerama support).
  * ACK 1.7.1-1.1 NMU, thanks to Steinar H. Gunderson (Closes: #349111).
  * Bug fix: "xbindkeys: It&#39;s not to be called "X Windows"
    ;-)", thanks to Jan Minar (Closes: #270207).
  * Bug fix from upstream: "xbindkeys doesn&#39;t work", thanks to BaBL
    (Closes: #405015).
  * Update description with the new guile stuff.

 -- Joerg Jaspert <joerg@debian.org>  Sat, 15 Sep 2007 00:29:59 +0200

xbindkeys (1.7.1-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Replace build-dependency on xlibs-dev with an explicit build-dependency
    on each required package. (Closes: #346820)

 -- Steinar H. Gunderson <sesse@debian.org>  Sat, 21 Jan 2006 02:36:25 +0100

xbindkeys (1.7.1-1) unstable; urgency=low

  * New Upstream.
    - Add guile/scheme configuration file style.
    - Some minor fixes.
  * Added guile-1.6-dev to Build-Depends.
  * Standards 3.6.1.0.

 -- Joerg Jaspert <joerg@debian.org>  Thu, 11 Mar 2004 22:08:30 +0100

xbindkeys (1.6.4-1) unstable; urgency=low

  * New Upstream release
  * Standards 3.5.10 now

 -- Joerg Jaspert <joerg@debian.org>  Sun,  8 Jun 2003 13:59:18 +0200

xbindkeys (1.6.3-1) unstable; urgency=low

  * New Upstream release.
    - Remove chdir ("/tmp")  in daemonize function.

 -- Joerg Jaspert <joerg@debian.org>  Fri, 15 Nov 2002 16:12:09 +0100

xbindkeys (1.6.1-1) unstable; urgency=low

  * New Upstream Version:
    - Minor buggfix : use strtol instead atoi for use with
      hexadecimal number.
    - Complete daemonzie function

 -- Joerg Jaspert <joerg@debian.org>  Tue, 12 Nov 2002 11:16:18 +0100

xbindkeys (1.6.0-1) unstable; urgency=low

  * New Upstream Version:
    - Release event for keys and buttons.
    - Don't use threads any more (use fork+exec instead)
    - Better daemonize function
    - Simplification of configure.in (better test for X)
    - Don't use sscanf any more
  * Upstream said this bug is no more (tested on some machines, some
    users with wmaker reported success to).
    I tested it with wmaker too. Plain standard config of wmaker, but it
    worked. So this (closes: #146215)
  * Standards 3.5.7 now.
  * Cleaned up rules file.

 -- Joerg Jaspert <joerg@debian.org>  Sun, 10 Nov 2002 22:31:17 +0100

xbindkeys (1.5.5-1) unstable; urgency=low

  * New Upstream Version:
    - Change: better identification of modifiers with --key and
      --mulitkey options.

 -- Joerg Jaspert <joerg@debian.org>  Thu, 16 May 2002 15:50:16 +0200

xbindkeys (1.5.4-2) unstable; urgency=low

  * Ok, now README.Debian describes better what to do if the Modifier
    Key Name is not known to xbindkeys. Thx to David B. Harris
    for the patch. This now really (closes: #146776)

 -- Joerg Jaspert <joerg@debian.org>  Mon, 13 May 2002 23:10:17 +0200

xbindkeys (1.5.4-1) unstable; urgency=low

  * New Upstream Version
    - add warning when a key is unknown in .xbindkeysrc.

 -- Joerg Jaspert <joerg@debian.org>  Mon, 13 May 2002 16:35:00 +0200

xbindkeys (1.5.3-1) unstable; urgency=low

  * New Upstream Version:
    - Bugfix: if the keycode is unknow then xbindkeys don't asign a
      key with keycode=0.
    - Change: The default RC file don't use the Menu key wich cause
      problems. (closes: #146130)
  * Added Text how to handle unknown Keys to README.Debian. (closes: #146776)
  * Corrected description, (closes: #146174)

 -- Joerg Jaspert <joerg@debian.org>  Mon, 13 May 2002 14:22:19 +0200

xbindkeys (1.5.2-2) unstable; urgency=low

  * Description changed. (closes: #146174)

 -- Joerg Jaspert <joerg@debian.org>  Tue,  7 May 2002 20:21:55 +0200

xbindkeys (1.5.2-1) unstable; urgency=low

  * New upstream version.
  * This release does not error out if a defined Key Shortcut is already
    in use. It just prints a warning, ignores the Shortcut and goes on to
    work with the rest of your Shortcuts. (closes: #145511)

 -- Joerg Jaspert <joerg@debian.org>  Sun,  5 May 2002 20:12:43 +0200

xbindkeys (1.5.1-1) unstable; urgency=low

  * Initial Release (closes: #145232)

 -- Joerg Jaspert <joerg@debian.org>  Tue, 30 Apr 2002 18:53:28 +0200

